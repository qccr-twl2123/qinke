# 社区出入登记系统

### 介绍
服务于疫情的社区出入登记系统，目标在于将线下通过纸笔记录信息的方式，改由微信小程序扫码。

### 系统架构
小程序前端基于uni框架编写，并且整合有赞vant前端开源组件进行编写<br/>
后端系统采用springboot-vue 前后端分离方式, 主要采用jeecg-boot为基础框架完成基础业务代码的编写.

### 部署指南
#### 小程序部署
使用HBuilderX 打卡项目源码,具体步骤如下：<br/>
1.配置微信小程序模拟器<br/>
 - 工具-设置-运行配置->小程序运行配置  <br/>
 - 1.2 设置微信开发者工具端口报权限：打开微信开发者工具----设置---代理设置----安全，默认是关闭的，选择开启服务端口<br/>
	
2.生成微信小程序代码<br/>	 
  -  在工具栏找到:运行->运行到小程序模拟器<br/>
  -  在该项目找到unpackage下找到mp-weixin<br/>
  -  在微信小程序上传代码，进入审核阶段<br/>

### 业务流程图
![业务流程图](/static/show/业务流程-1.png)

### 界面展示
#### 首页
![avatar](/static/show/首页.png)

####  扫码成功&扫码失败
![avatar](/static/show/验证成功页面.png)
![avatar](/static/show/验证失败页面.png)

####  完善信息页面
![avatar](/static/show/完善个人信息.png)





